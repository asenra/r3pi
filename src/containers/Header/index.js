import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Header from '../../components/Header'

const HeaderContainer = ({ products, title }) => {
  const totalProducts = Object.keys(products).reduce((acc, cur) => products[cur].total + acc, 0)

  return (
    <Header title={title} totalCartProducts={totalProducts} />
  )
}

HeaderContainer.propTypes = {
  products: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
}

export default connect()(HeaderContainer)
