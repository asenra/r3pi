import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import App from '../../components/App'

const AppContainer = ({ products }) => (
  <App products={products} />
)

AppContainer.propTypes = {
  products: PropTypes.object.isRequired,
}

const mapStateToProps = state => ({
  products: state.products,
})

export default connect(
  mapStateToProps,
)(AppContainer)
