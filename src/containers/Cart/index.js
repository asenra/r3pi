import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as productActions from '../../redux/actions'
import Cart from '../../components/Cart'

const CartContainer = ({ products, actions }) => {
  const totalPrice = Object.keys(products)
    .reduce((acc, cur) => {
      if (!products[cur].inDiscount) {
        return acc + (products[cur].total * products[cur].price)
      }
      // eslint-disable-next-line max-len
      return acc + ((products[cur].total - parseInt(products[cur].total / 3, 10)) * products[cur].price)
    }, 0)

  return (
    <Cart
      products={products}
      totalPrice={totalPrice}
      removeProduct={actions.removeProduct}
    />
  )
}

CartContainer.propTypes = {
  products: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired,
}

const mapStateToProps = state => ({
  products: state.products,
})

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(productActions, dispatch),
})

export default connect(mapStateToProps, mapDispatchToProps)(CartContainer)
