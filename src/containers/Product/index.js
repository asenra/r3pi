import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Product from '../../components/ProductList/Product'
import * as productActions from '../../redux/actions'

const ProductContainer = props => (
  <Product {...props} />
)

const mapStateToProps = state => ({
  products: state.products,
})

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(productActions, dispatch),
})

export default connect(mapStateToProps, mapDispatchToProps)(ProductContainer)
