import * as types from '../actionTypes'

export const addProduct = product => ({
  type: types.ADD_PRODUCT,
  product,
})

export const removeProduct = product => ({
  type: types.REMOVE_PRODUCT,
  product,
})
