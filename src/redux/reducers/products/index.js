import { ADD_PRODUCT, REMOVE_PRODUCT } from '../../actionTypes'

const initialState = {
  apple: {
    name: 'Apple',
    price: 0.25,
    inDiscount: false,
    total: 0,
  },
  orange: {
    name: 'Orange',
    price: 0.3,
    inDiscount: false,
    total: 0,
  },
  banana: {
    name: 'Banana',
    price: 0.1,
    inDiscount: false,
    total: 0,
  },
  papaya: {
    name: 'Papaya',
    price: 0.5,
    inDiscount: true,
    total: 0,
  },
}

const getProduct = (products, name) =>
  products[Object.keys(products).filter(product => product === name.toLowerCase())]

const products = (state = initialState, action) => {
  const selectedProduct = action.product && getProduct(state, action.product)

  switch (action.type) {
    case ADD_PRODUCT:
      selectedProduct.total++

      return ({
        ...state,
        [action.product.toLowerCase()]: { ...selectedProduct },
      })

    case REMOVE_PRODUCT:
      selectedProduct.total = selectedProduct.total === 0 ? 0 : selectedProduct.total - 1

      return ({
        ...state,
        [action.product.toLowerCase()]: { ...selectedProduct },
      })

    default:
      return state
  }
}

export default products
