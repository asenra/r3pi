import React from 'react'
import PropTypes from 'prop-types'
import Header from '../../containers/Header'
import ProductList from '../ProductList'
import Cart from '../../containers/Cart'
import './style.scss'

const App = ({ products }) => (
  <div className="App">
    <Header title="Shopping Cart" products={products} />
    <ProductList />
    <Cart products={products} />
  </div>
)

App.propTypes = {
  products: PropTypes.object.isRequired,
}

export default App
