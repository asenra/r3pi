import React from 'react'
import PropTypes from 'prop-types'
import CartRow from './CartRow'
import './style.scss'

const Cart = ({ products, totalPrice, removeProduct }) => {
  const productsInCart = Object.keys(products)
    .filter(product => products[product].total > 0)
    .map(product => (
      <CartRow
        key={products[product].name}
        image="imagem"
        quantity={products[product].total}
        title={products[product].name}
        price={products[product].price}
        handleRemoveClick={removeProduct}
      />
    ))

  return (
    <div className="cart">
      <div className="row row-header">
        <div className="image">Image</div>
        <div className="quantity">Quantity</div>
        <div className="title">Title</div>
        <div className="remove">Remove</div>
        <div className="price">Price</div>
      </div>
      {productsInCart.length > 0 ? productsInCart : 'There are no products in your cart.'}
      <div className="row row-total">
        <div className="total">Total</div>
        <div className="total-value">{totalPrice.toFixed(2)} CHF</div>
      </div>
    </div>
  )
}

Cart.propTypes = {
  products: PropTypes.object.isRequired,
  totalPrice: PropTypes.number.isRequired,
  removeProduct: PropTypes.func.isRequired,
}

export default Cart
