import React from 'react'
import PropTypes from 'prop-types'
import './style.scss'

const removeIcon = require('./remove.png')

const apple = require('./productImages/apple.png')
const banana = require('./productImages/banana.png')
const orange = require('./productImages/orange.png')
const papaya = require('./productImages/papaya.png')

const PRODUCT_IMAGES = {
  apple,
  banana,
  orange,
  papaya,
}

const CartRow = ({ quantity, title, price, handleRemoveClick }) => {
  const handleClick = () => handleRemoveClick(title)

  return (
    <div className="row cart-row">
      <div className="image"><img src={PRODUCT_IMAGES[title.toLowerCase()]} alt={title} /></div>
      <div className="quantity">{quantity}</div>
      <div className="title">{title}</div>
      <div className="remove">
        <img src={removeIcon} alt="Remove" onClick={handleClick} />
      </div>
      <div className="price">{price} CHF</div>
    </div>
  )
}

CartRow.propTypes = {
  quantity: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  handleRemoveClick: PropTypes.func.isRequired,
}

export default CartRow
