import React from 'react'
import Product from '../../containers/Product'
import './style.scss'

const PRODUCTS = [
  {
    name: 'Apple',
    price: 0.25,
    inDiscount: false,
  },
  {
    name: 'Orange',
    price: 0.30,
    inDiscount: false,
  },
  {
    name: 'Banana',
    price: 0.10,
    inDiscount: false,
  },
  {
    name: 'Papaya',
    price: 0.50,
    inDiscount: true,
  },
]

const ProductList = () => (
  <div className="product-list">
    {
      PRODUCTS.map(product => <Product key={product.name} {...product} />)
    }
  </div>
)

export default ProductList
