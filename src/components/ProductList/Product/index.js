import React from 'react'
import PropTypes from 'prop-types'
import './style.scss'

const cartIcon = require('./cart.png')
const discountIcon = require('./discount.png')

const apple = require('./productImages/apple.png')
const banana = require('./productImages/banana.png')
const orange = require('./productImages/orange.png')
const papaya = require('./productImages/papaya.png')

const PRODUCT_IMAGES = {
  apple,
  banana,
  orange,
  papaya,
}

const Product = ({ name, price, inDiscount, actions }) => {
  const handleClick = () => actions.addProduct(name)

  return (
    <div className="product">
      <img src={PRODUCT_IMAGES[name.toLowerCase()]} alt={name} />
      <div className="product-name">
        {name}
        {inDiscount &&
          <img
            src={discountIcon}
            className="discount-price"
            alt="3 for 2 discount!"
          />
        }
      </div>
      <div className="product-price">{price} CHF</div>
      <div className="add-to-cart">
        <img
          src={cartIcon}
          className="cart-icon"
          alt="Add to cart."
          onClick={handleClick}
        />
      </div>
    </div>
  )
}

Product.propTypes = {
  name: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  inDiscount: PropTypes.bool.isRequired,
  actions: PropTypes.object.isRequired,
}

export default Product
