import React from 'react'
import PropTypes from 'prop-types'
import './style.scss'

const r3piLogo = require('./r3pi-logo.png')
const shoppingCart = require('./shopping-cart.png')

const Header = ({ title, totalCartProducts }) => (
  <div className="app-header">
    <img
      src={r3piLogo}
      className="logo"
      alt="r3pi"
    />
    <h2>{title}</h2>
    <div className="shopping-cart">
      <img
        src={shoppingCart}
        className="shopping-cart"
        alt="Your cart"
      />
      {totalCartProducts} items on your cart
    </div>
  </div>
)

Header.propTypes = {
  title: PropTypes.string.isRequired,
  totalCartProducts: PropTypes.number.isRequired,
}

export default Header
