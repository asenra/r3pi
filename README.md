# R3PI Shopping Cart
![R3PICart](https://image.ibb.co/mUOgyQ/r3picart.png)
Small shopping cart prototype built for the R3PI interview process, using:
- React
- Redux
- Webpack
- SASS
- ES6
- ESLint

# How to run it

  - run ```npm install``` to install all the dependencies
  - run ```npm start``` to fire up th webpack dev server
  - you can access the application on your browser at ```http://localhost:8080/```


Contact:
  - ```andre-senra@hotmail.com```